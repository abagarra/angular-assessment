import { Component } from '@angular/core';
import { LoggerService } from './logger/logger.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Assessment';

  constructor(private log: LoggerService) {
    this.log.log('AppComponent', 'got a collection of films to display')

  }

}
